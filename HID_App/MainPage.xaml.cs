﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.Devices.Enumeration;
using Windows.Devices.HumanInterfaceDevice;
using Windows.Storage;
using Windows.Storage.Streams;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HelloWorld
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void searchHIDButtonClick(object sender, RoutedEventArgs e)
        {
            USBFound.Text = "start Search";
            enumerateHidDevices();
            
        }

        HidDevice device;

        // Enumerate HID devices
        private async void enumerateHidDevices()
        {
            ushort vendorId = 0x04D8;
            ushort productId = 0x003F;
            ushort usagePage = 0xFF00;
            ushort usageId = 0x0001;

            // Create a selector that gets a HID device using VID/PID and a 
            // VendorDefined usage
            string selector = HidDevice.GetDeviceSelector(usagePage, usageId, vendorId, productId);

            // Enumerate devices using the selector
            var devices = await DeviceInformation.FindAllAsync(selector);

            if (devices.Count > 0)
            {
                // Open the target HID device
                device = await HidDevice.FromIdAsync(devices.ElementAt(0).Id, FileAccessMode.ReadWrite);

                // At this point the device is available to communicate with
                // So we can send/receive HID reports from it or 
                // query it for control descriptions
                USBFound.Text = "device found";
            }
            else
            {
                // There were no HID devices that met the selector criteria
                USBFound.Text = "device not found";
            }
        }

        private async void sendOutputReportAsync(byte reportId, byte firstByte)
        {
            var outputReport = device.CreateOutputReport(reportId);

            var dataWriter = new DataWriter();

            // First byte is always the report id
            dataWriter.WriteByte((Byte)outputReport.Id);
            dataWriter.WriteByte(firstByte);
            dataWriter.WriteBytes(new byte[63]);

            outputReport.Data = dataWriter.DetachBuffer();

            uint bytesWritten = await device.SendOutputReportAsync(outputReport);
        }

        private async void getNumericInputReportAsync()
        {
            var inputReport = await device.GetInputReportAsync(0x37);

            var data = inputReport.Data;

            var dataReader = DataReader.FromBuffer(data);

            byte[] bytesRead = new byte[64];
            dataReader.ReadBytes(bytesRead);

            //bytesRead[2] and [3] when working without report ID's in report description - else [1] and [2] 
            int pot = bytesRead[2] == 1 ? 1023 : 0;//bytesRead[2] + 256 * bytesRead[3];
            potProgressBar.Value = pot;
        }

        private void outputReportButtonClick(object sender, RoutedEventArgs e)
        {
            sendOutputReportAsync(0x80, 0x80);
        }

        private void inputReportButtonClick(object sender, RoutedEventArgs e)
        {
            getNumericInputReportAsync();
        }
    }
}
